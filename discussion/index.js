// Express Setup

const express = require ("express")

// Mongoose is a package that allows creation of schemas to model our data istructure
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [Section] - Mongoose Connection
//Rename (/Activity?)
// (Mongoose uses the 'connect' function to connect to the cluster in our MongoDB Atlas)

/*
	It takes 2 arguments:
	1. Connection string fro our MongoDB Atlas.
	2. Object that contains middlewares/ statndards that MongoDB uses.
*/

// To connect MongoDB
mongoose.connect(`mongodb+srv://jaypsam:admin123@zuitt-batch197.fxakvar.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
	// {newUrlParser: true} - it allows us to avoid current and/ or future error while connecting to MongoDB
	useNewUrlParser: true,
	// useUnifiedTopology if "false" by default. Set to true to opt in to using the MongoDB driver's new connection management engine
	useUnifiedTopology: true
})

// Initializes the mongoose connection to the mongoDB Db by assigning 'mongoose.connection' to the 'db' variable.
let db = mongoose.connection

// Lister to the events of the connection by using the 'on()' function of the mongoose connection and logs the details in the console based on event
// Confirmation message
db.on('error', console.error.bind(console, "Connection"))
db.once('open', () => console.log('Connection to MongoDB!'))


app.use(express.urlencoded({extended:true}));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//CREATING A SCHEMA
// Used for blueprint
const taskSchema = new mongoose.Schema({
	// Define the fields with their corresponding data types
	// For a task, it needs a "task name" and its data type will be "String"
	name: String,
	// There is a field called "status" that has a data type of "String" and the default value is "pending"
	status: {
		type: String,
		default: 'Pending'
	}
})

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// MODELS
// The variable/ object "Task" can now be used to run command for interacting with our DB.
// "Task" is capitalized following the MVC approach for the naming conventions
// Models must be in singular form and capitalized
// The first paremeter is used to specify the Name of the collection where we will store our data
// The second parameter is used to specify the Schema/ blueprint of the documents that will be stored in our MongoDB collection/s
const Task = mongoose.model('Task', taskSchema);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// CREATING THE ROUTES

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Middleware
// To allow to read application
app.use(express.json());
// To allow data in the forms
app.use(express.urlencoded({extended:true}));


// Route handling
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Create a user route
app.post('/tasks', (req, res) => {
	// business logic
	Task.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name){
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task({
				name: req.body.name
			})
			newTask.save((error, savedTask) => {
				if (error){
					return console.error(error)
				} else {
					return res.status(201).send('New Task Created')
				}
			})
		}
	})
})

// Get all users from collection - get all users route
app.get('/tasks', (req, res) => {
	Task.find({}, (error, result) => {
		if(error){
			return res.send(error)
		} else{
			return res.status(200).json({
				tasks: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at port: ${port}`))



