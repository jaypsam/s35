// Express Setup

const express = require ("express")
const mongoose = require("mongoose");

const app = express();
const port = 4001;

// To connect MongoDB
mongoose.connect(`mongodb+srv://jaypsam:admin123@zuitt-batch197.fxakvar.mongodb.net/S35-Activity2?retryWrites=true&w=majority`, {

	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', console.error.bind(console, "Connection"))
db.once('open', () => console.log('Connection to MongoDB!'))


app.use(express.urlencoded({extended:true}));

// Schema
const signUpSchema = new mongoose.Schema({
	name: String,
	password: String

})

const SignUp = mongoose.model('SignUp', signUpSchema);
// Routes
// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// User route
app.post('/signups', (req, res) => {
	SignUp.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name){
			return res.send('Duplicate task found')
		} else {
			let newSignUp = new SignUp({
				name: req.body.name,
				password: req.body.password
			})
			newSignUp.save((error, savedSignUp) => {
				if (error){
					return console.error(error)
				} else {
					return res.status(201).send('New user registered')
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))